#!/usr/bin/python3

import os
from datetime import datetime, timezone
from dateutil import tz
from lxml import html

for d in os.scandir('.'):
    if d.is_dir():
        post = html.parse(os.path.join(d, 'index.html')).getroot()
        orig_date = post.cssselect('span#date')
        if len(orig_date) == 1:
            orig_date = orig_date[0]
        else:
            main = post.cssselect('#main')[0]
            p = main.makeelement('p')
            p.set('class', 'date')
            span = p.makeelement('span')
            span.set('id', 'date')
            span.text = '0'
            p.append(span)
            p.tail = "\n\t"
            main.insert(0, p)
            orig_date = span
        try:
            date = datetime.strptime(orig_date.text, '%Y.%m.%d. %H:%M')
            date.replace(tzinfo=tz.gettz())
            newdate = date.astimezone(tz.gettz('Asia/Seoul')).strftime('%Y-%m-%d %H:%M')
        except ValueError:
            continue
        orig_date.text = newdate
        f = open(os.path.join(d, 'index.html'), 'w')
        f.write(html.tostring(post, pretty_print=True, encoding='unicode', doctype='<!DOCTYPE html>'))
        f.close()
