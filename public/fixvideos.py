#!/usr/bin/python3

import os
import sys
from datetime import datetime, timezone
from dateutil import tz
from lxml import html

index = html.parse(sys.argv[1]).getroot()
main = index.cssselect('#main')[0]

class Vid():
    def __init__(self, title, date, url):
        self.title = title
        self.date = date
        self.url = url

vids = []

for v in main.cssselect('p'):
    span = v.cssselect('.date')[0]
    date = span.text
    title = span.tail.strip()
    url = v.cssselect('a')[0].get('href')
    try:
        olddate = datetime.strptime(date, '%Y.%m.%d. %H:%M')
        olddate.replace(tzinfo=tz.gettz())
        newdate = olddate.astimezone(tz.gettz('Asia/Seoul')).strftime('%Y-%m-%d %H:%M')
    except ValueError:
        newdate = date
    vids.append(Vid(title, newdate, url))
    v.getparent().remove(v)

vids.sort(key=lambda v: datetime.strptime(v.date, '%Y-%m-%d %H:%M'))

for v in vids:
    p = main.makeelement('p')
    a = p.makeelement('a')
    a.set('href', v.url)
    span = a.makeelement('span')
    span.set('class', 'date')
    span.text = v.date
    span.tail = ' '+v.title
    a.append(span)
    p.append(a)
    main.append(p)

f = open(sys.argv[1], 'w')
f.write(html.tostring(index, pretty_print=True, encoding='unicode', doctype='<!DOCTYPE html>'))
f.close()
