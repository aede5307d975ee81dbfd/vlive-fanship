#!/usr/bin/python3

import os
from lxml import html
from lxml.cssselect import CSSSelector

index = html.parse('template.html').getroot()
main = index.cssselect('#main')[0]
g = open('group.txt')
gn = g.read().strip()
g.close()
index.cssselect('head>title')[0].text = '{} fanship posts'.format(gn)
d = index.cssselect('p.date')[0]
d.getparent().remove(d)

dirs = []
for d in os.scandir('.'):
    if d.is_dir():
        dirs.append(d)

dirs.sort(key=lambda d: int(d.name))

for d in dirs:
    post = html.parse(os.path.join(d, 'index.html')).getroot()
    title = post.cssselect('head>title')[0].text
    p = main.makeelement('p')
    a = p.makeelement('a')
    a.set('href', d.name)
    span = a.makeelement('span')
    span.set('class', 'date')
    date = '0'
    postdate = post.cssselect('span#date')
    if len(postdate) == 1:
        postdate = postdate[0].text
        date = postdate
    span.text = date
    span.tail = ' '+title
    a.append(span)
    p.append(a)
    main.append(p)

f = open('index.html', 'w')
f.write(html.tostring(index, pretty_print=True, encoding='unicode', doctype='<!DOCTYPE html>'))
f.close()
